import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';
import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { FixedPluginModule} from './shared/fixedplugin/fixedplugin.module';
//import { NguiMapModule} from '@ngui/map';
import { HttpModule } from '@angular/http'
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { UserComponent }   from './user/user.component';
import { TableComponent }   from './table/table.component';
import { TypographyComponent }   from './typography/typography.component';
import { IconsComponent }   from './icons/icons.component';
import { NotificationsComponent }   from './notifications/notifications.component';
import { UpgradeComponent }   from './upgrade/upgrade.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { CurrencyMaskModule } from "ng2-currency-mask";

import { GeralService } from './geral.service';
import { ProdutoComponent } from './produto/produto.component';
import { VendaComponent } from './venda/venda.component';
import { FinanceiroComponent } from './financeiro/financeiro.component';

import { TextMaskModule } from 'angular2-text-mask';

import { TypeaheadModule, AlertModule } from 'ngx-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    UserComponent,
    TableComponent,
    TypographyComponent,
    IconsComponent,
    NotificationsComponent,
    UpgradeComponent,
    ProdutoComponent,
    VendaComponent,
    FinanceiroComponent,
  ],
  imports: [
    AngularFontAwesomeModule,
    TextMaskModule,
    BrowserModule,
    CurrencyMaskModule,
    BrowserAnimationsModule,
    TypeaheadModule.forRoot(),
    AlertModule.forRoot(),
    RouterModule.forRoot(AppRoutes, { useHash: true }),
    SidebarModule,
    NavbarModule,
    FooterModule,
    FormsModule,
    FixedPluginModule,
    HttpModule,
    HttpClientModule,
    //NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=YOUR_KEY_HERE'})

  ],
  providers: [GeralService],
  bootstrap: [AppComponent]
})
export class AppModule { }
