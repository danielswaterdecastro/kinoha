import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { ClienteModel } from '../models/clienteModel';
import { ProdutoModel } from './../models/produtoModel';
import { GeralService } from './../geral.service';
import { FabricanteModel } from './../models/fabricanteModel';
import { FornecedorModel } from './../models/fornecedorModel';
import { VendaModel } from './../models/vendaModel';

import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

declare var $:any;

@Component({
  selector: 'app-venda',
  templateUrl: './venda.component.html',
  styleUrls: ['./venda.component.css']
})
export class VendaComponent implements OnInit {
    @ViewChild('new') public new:ElementRef;
    public mask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
    titulo: string;
    clienteModel: ClienteModel;
    clienteArray: ClienteModel[];
    clienteSituacao: any;
    clienteValue: any;
    inputDataRetroativa: boolean = false;
    habilitarCampoCliente: boolean;
    habilitarCamposProduto: boolean;
    habilitarCamposFabricante: boolean;
    produtoModel: ProdutoModel;
    produtoArray: ProdutoModel;
    painelCliente: boolean;
    painelProduto: boolean;
    produtoSituacao: any;
    quantidadeValue: any;
    fabricanteModel: FabricanteModel;
    fornecedorModel: FornecedorModel;
    vendaModel: VendaModel;
    formaPagamentoValue: any;
    reciboValue: any;
    classeMensagem: any;
    mensagem: string;
    alerta: boolean;
    quitadoValue: any;
    quitadoArray: Array<any> = [];
    tipoData: Array<any> = [];
    tipoDataValue: any;
    //vendaArray: VendaModel = [];
    vendaArray: Array<VendaModel> = [];
    reciboArray: Array<any> = [];
    formaPagamentoArray = [];
    recibo: boolean = false;
    print: boolean = false;
    habilitaFormaPagamento: boolean = false;
    habilitarCarrinho: boolean = false;
    btnAdicionaProduto: boolean = true;
    btnFinalizarVenda: boolean = true;
    btnImprimir: boolean = false;
    btnNovaVenda: boolean = true;
    formaPagamento: string;
    btnRecalcularPercent: boolean = false;
    btnCalcularPercent: boolean = true;
    descontoPercent: number;

    constructor(
      private route: ActivatedRoute,
      private service: GeralService,
      ) {
        this.clienteSituacao = null;
        this.clienteValue = null;
        this.reciboValue = null;
        this.quitadoValue = null;
        this.formaPagamentoValue = null;
        this.tipoDataValue = null;
        this.clienteModel = new ClienteModel();
        this.produtoModel = new ProdutoModel();
        this.fabricanteModel = new FabricanteModel();
        this.fornecedorModel = new FornecedorModel();
        this.vendaModel = new VendaModel();
        this.fabricanteModel.nome_fabricante = 'Fabricante';
        this.fornecedorModel.nome_fornecedor = 'Fornecedor';
        this.produtoSituacao = null;
        this.route.params.subscribe( params => {
          if(params.id){
            this.titulo = "Editar Cliente";
          }
          else{
            this.titulo = "Venda";
          }
        });
      }

      ngOnInit() {
        this.tipoData = [{
          label: 'Atual',
          id: 1
        },
        {
          label: 'Retroativa',
          id: 2
        }
      ]
        this.quitadoArray = [{
          label: 'Sim',
          value: 'Sim'
        },
        {
          label: 'Não',
          value: 'Não'
        }
      ]
      this.formaPagamentoArray = [{
        label: 'Crédito',
        value: 'Crédito'
      },
      {
        label: 'Débito',
        value: 'Débito'
      },
      {
        label: 'Dinheiro',
        value: 'Dinheiro'
      },
      {
        label: 'Cheque',
        value: 'Cheque'
      },
      {
        label: 'Boleto',
        value: 'Boleto'
      },
      {
        label: 'Outros',
        value: 'Outros'

      }
    ]
    this.getClienteAtivo();
    this.getTodasVendas();
  }

  getTipoData(event){
    if(event == 2){
      this.inputDataRetroativa = true;
      this.vendaModel.data_retroativa = null;
    }
  }

  getClienteAtivo(){
    this.service.getClienteSituacao(0).subscribe(data => {
      this.clienteArray = data['clientes'];
    })
  }

  getProdutoSituacao(){
    this.service.getProdutoSituacao(0).subscribe(data => {
      this.produtoArray = data['produtos'];
    })
  }

  getClienteCadastrado(event){
    if(event !== 'null'){
      this.habilitarCamposProduto = true;
      this.getClienteId(event);
      this.getProdutoSituacao();
    }
    else{
      this.habilitarCamposProduto = false;
      this.produtoSituacao = null;
    }

  }

  getClienteId(id){
    this.service.getClienteId(id).subscribe((data: ClienteModel) => {
      this.clienteModel = data['cliente'];
    })
  }

  getProdutoCadastrado(event){
    console.log(event);
    if(event !== 'null'){
      this.recibo = true;
      this.print = false;
      this.reciboValue = null;
      this.getFabricanteNome(event);
      this.getFornecedorNome(event);

      this.service.getProdutoId(event).subscribe(data => {
        this.produtoModel = data['produto'];
        for(let i = 0; i < this.vendaArray.length; i++){
          if(this.vendaArray[i].id_produto == this.produtoModel.id){
            alert('Produto já adicionado a cesta!');
            return;
          }
        }
        this.vendaArray.push({
          id_produto: this.produtoModel.id,
          codigo: this.produtoModel.codigo,
          estoque: this.produtoModel.estoque,
          quantidade: this.quantidadeValue,
          nome_produto: this.produtoModel.nome,
          preco_custo: this.produtoModel.preco_custo,
          preco_venda: this.produtoModel.preco_venda,
          valor_subtotal: this.vendaModel.valor_subtotal,
          nome_fabricante: this.fabricanteModel.nome_fabricante,
          nome_fornecedor: this.fornecedorModel.nome_fornecedor,
        });
        this.habilitarCampoCliente = true;
        this.habilitarCamposProduto = false;
      })
    }
  }

  getFabricanteNome(idProduto){
    return this.service.getFabricanteNome(idProduto).subscribe((data: FabricanteModel) => {
      this.fabricanteModel.nome_fabricante = data['fabricante'][0]['nome_fabricante'];
    })
  }

  getFornecedorNome(idProduto){
    return this.service.getFornecedorNome(idProduto).subscribe((data: FornecedorModel) => {
      console.log(data);
      this.fornecedorModel.nome_fornecedor = data['fornecedor'][0]['nome_fornecedor'];
    })
  }

  adicionaProduto(){
    this.habilitarCampoCliente = true;
    this.habilitarCarrinho = true;
    this.produtoSituacao = null;
    this.habilitarCamposProduto = true;
    this.new.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'start' });
  }

  calculaSubtotal(event, index){
    this.vendaModel.valor_total = 0;
    this.vendaArray[index].valor_subtotal = this.vendaArray[index].preco_venda * event;
    for(let i = 0; i < this.vendaArray.length; i++){
      this.vendaModel.valor_total = this.vendaModel.valor_total + this.vendaArray[i].valor_subtotal;
    }
    return this.vendaModel.valor_total;
  }

  calculaPorcentagem(){
    let percent;
    sessionStorage.setItem('valorTotal', this.vendaModel.valor_total);
    if(this.descontoPercent != undefined){
      percent = (this.vendaModel.valor_total * this.descontoPercent) / 100;
      this.vendaModel.valor_total = this.vendaModel.valor_total - percent;
      this.btnCalcularPercent = false;
      this.btnRecalcularPercent = true;
    }
  }

  recalculaPorcentagem(){
    this.vendaModel.valor_total = sessionStorage.getItem('valorTotal');
    this.btnCalcularPercent = true;
    this.btnRecalcularPercent = false;
    this.descontoPercent = null
  }

  removerProduto(event, index){
    this.vendaModel.valor_total = this.vendaModel.valor_total - this.vendaArray[index].valor_subtotal;
    this.vendaArray.splice(index, 1);
  }

  getFormaPagamento(event){
    this.formaPagamento = '';
    if(event == 'Outros'){
      this.habilitaFormaPagamento = true;
    }
    this.formaPagamento = event;
  }

  finalizarVenda(){
    if(!this.vendaArray.length){
      alert("Adicione um produto");
      this.produtoSituacao = null;
      this.habilitarCamposProduto = true;
      return;
    }
    for(let i = 0; i < this.vendaArray.length; i++){
      if(this.vendaArray[i].quantidade == undefined){
        alert('O campo quantidade é obrigatório');
        return;
      }
      if(this.vendaArray[i].quantidade == 0){
        alert('O campo quantidade não pode ser igual a zero');
        return;
      }
      if(this.vendaArray[i].quantidade > this.vendaArray[i].estoque){
        alert('A quantidade de produto ' + this.vendaArray[i].nome_produto + ' é maior do que a quantidade em estoque.');
        return;
      }
    }
    if(this.formaPagamentoValue == null || this.formaPagamentoValue == 'null'){
      alert('Selecione uma forma de pagamento');
      return;
    }
    if(this.formaPagamentoValue == 'outros' && this.vendaModel.informacao_forma_pagamento == undefined){
      alert('É necessário preencher as informações sobre fomra de pagamento.');
      return;
    }
    if(this.vendaModel.nome == null || this.vendaModel.nome == ""){
      alert('Adicione uma breve descrição para a venda');
      return;
    }
    if(this.quitadoValue == null || this.quitadoValue == null){
      alert("Selecione se a venda está quitada ou não.");
      return;
    }
    if(this.tipoDataValue == null){
      alert("Selecione um tipo de data.");
      return;
    }
    if(this.tipoDataValue == 2 && this.vendaModel.data_retroativa == null){
      alert("Informe uma data retroativa.");
      return;
    }
    this.vendaModel.id_cliente = this.clienteModel.id;
    this.vendaModel.forma_pagamento = this.formaPagamentoValue;
    this.vendaModel.produtoArray = this.vendaArray;
    this.service.postVenda(this.vendaModel).subscribe((response) => {
      if(response['sucesso']){
        this.btnAdicionaProduto = false;
        this.btnFinalizarVenda = false;
        this.btnNovaVenda = true;
        this.verificaMensagemAlerta(true, response['mensagem']);
        this.getTodasVendas();
        this.getRecibo(response['venda']);
      }
      else{
        this.verificaMensagemAlerta(false, response['mensagem']);
      }
    })
  }

  novaVenda(){
    this.vendaArray = [];
    this.btnAdicionaProduto = true;
    this.btnFinalizarVenda = true;
    this.recibo = false;
    this.reciboValue = null;
    this.habilitarCampoCliente = false;
    this.habilitarCamposProduto = true;
    this.tipoDataValue = null;
    this.inputDataRetroativa = false;
    this.clienteValue = null;
    this.quitadoValue = null;
    this.produtoSituacao = null;
    this.formaPagamentoValue = null;
    this.habilitaFormaPagamento = false;
    this.vendaModel.informacao = '';
    this.vendaModel.informacao_forma_pagamento = '';
    this.vendaModel.nome = '';

    if(this.print){
      this.print = false;
    }
    if(this.alerta){
      this.alerta = false;
    }
  }

  getTodasVendas(){
    this.service.getTodasVendas().subscribe(data => {
      this.reciboArray = data['vendas'];
    })
  }

  getRecibo(event){
    if(event == 'null'){
      this.print = false;
      return;
    }
    this.vendaArray = [];
    this.recibo = false;
    this.print = true;

    this.habilitarCampoCliente = false;
    this.habilitarCamposProduto = true;

    this.clienteValue = null;
    this.produtoSituacao = null;

    this.service.getRecibo(event).subscribe(data => {
      this.vendaModel = data['venda'][0];
      this.getFormaPagamento(this.vendaModel.forma_pagamento);
      this.vendaModel.produtoArray = data['produtos'];
      //this.reciboArray = data['vendas'];
    });

  }

  getQuitado(event){
    this.vendaModel.quitado = event;
  }

  public captureScreen(){
    var data = document.getElementById('print');
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF

      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('-recibo.pdf'); // Generated PDF
    });
  }

  verificaMensagemAlerta(tipo: boolean, msg: string){
    if(tipo){
      this.classeMensagem = 'alert-success';
      this.alerta = true;
    }
    else{
      this.classeMensagem = 'alert-danger';
      this.alerta = true;
    }
    this.mensagem = msg;
  }
}
