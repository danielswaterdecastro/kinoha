import { Component, OnInit } from '@angular/core';

import {ActivatedRoute} from "@angular/router";
import { ClienteModel } from '../models/clienteModel';
import { GeralService } from './../geral.service';

@Component({
  selector: 'app-financeiro',
  templateUrl: './financeiro.component.html',
  styleUrls: ['./financeiro.component.css']
})
export class FinanceiroComponent implements OnInit {
  
  clienteModel: ClienteModel;
  clienteValue: any;
  clienteSituacao: any;
  clienteArray: ClienteModel[];
  tabelaVendaCliente: boolean = false;
  tabelaVendaProduto: boolean = false;
  tabelaVenda: Array<any> = [];
  tabelaVendaMaisVendido: boolean =  false;  
  tabelaProdutoMaisVendido: Array<any> = [];
  totalLucroQuitado:any;
  totalLucroPendente: any;
  produtoMaisVendido: Array<any> = [];
  dataInicial: any;
  dataFinal: any;
  
  constructor(
    private route: ActivatedRoute,
    private service: GeralService,
    ) {
      this.clienteValue = null;
      this.clienteSituacao = null;
      this.clienteModel = new ClienteModel();
    }
    
    ngOnInit() {
      this.getProdutoMaisVendido();
      this.getClienteSituacao(0);
    }
    
    getClienteSituacao(event?){
      this.clienteValue = null;
      if(event === 'null'){            
        //this.getTodosClientes();
        return;
      }
      else{
        this.service.getClienteSituacao(parseInt(event)).subscribe(data => {
          this.clienteArray = data['clientes'];
        })
      }
    }
    
    getVendaPorCliente(event){
      console.log('event', event)
      this.service.getVendaByCliente(event).subscribe(data => {
        this.tabelaVenda = data['venda'];
        this.tabelaVendaCliente = true;
        this.totalLucroQuitado = 0;
        this.totalLucroPendente = 0;
        for(let i = 0; i < this.tabelaVenda.length; i++){
          if(this.tabelaVenda[i].quitado == "Sim"){
            this.totalLucroQuitado = parseInt(this.totalLucroQuitado) + parseInt(this.tabelaVenda[i].valor_subtotal);
          }
          else{
            this.totalLucroPendente = parseInt(this.totalLucroPendente) + parseInt(this.tabelaVenda[i].valor_subtotal);
          }
        }
      })
      
    }
    
    quitarVenda(item){
      console.log('item', item);
      if(window.confirm("Tem certeza que deseja quitar a venda? Após confirmar essa ação, não será possível desfazer")){            
        this.service.postQuitarVenda(item).subscribe((response) => {
          if(response['sucesso']){
            this.getVendaPorCliente(item.id_cliente);
          }
        })
      }
      
    }

    getProdutoMaisVendido(){
      this.service.getProdutoMaisVendido().subscribe(data => {
        this.produtoMaisVendido = data['produtos'];
      })
    }

    filtrarData(){
      var datas = 
        {
          dataInicial: this.dataInicial,
          dataFinal: this.dataFinal
        }
      
      this.service.postProdutoData(datas).subscribe((data) => {
        console.log('data', data['venda']);
        this.tabelaVenda = data['venda'];
        this.tabelaVendaCliente = true;
        this.totalLucroQuitado = 0;
        this.totalLucroPendente = 0;
        for(let i = 0; i < this.tabelaVenda.length; i++){
          if(this.tabelaVenda[i].quitado == "Sim"){
            this.totalLucroQuitado = parseInt(this.totalLucroQuitado) + parseInt(this.tabelaVenda[i].valor_subtotal);
          }
          else{
            this.totalLucroPendente = parseInt(this.totalLucroPendente) + parseInt(this.tabelaVenda[i].valor_subtotal);
          }
        }
      })
    }
    
  }
  