import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { ProdutoModel } from '../models/produtoModel';
import { FabricanteModel } from '../models/fabricanteModel';
import { FornecedorModel } from '../models/fornecedorModel';
import { GeralService } from './../geral.service';

declare var $:any;

@Component({
  selector: 'app-produto',
  moduleId: module.id,
  templateUrl: './produto.component.html'
})
export class ProdutoComponent implements OnInit {

  titulo: string;
  produtoModel: ProdutoModel;
  fabricanteModel: FabricanteModel;
  fornecedorModel: FornecedorModel;
  _abrirFormNovoProduto: boolean = false;
  produtoArray: ProdutoModel[];
  produtoSituacao: any;
  fornecedorSituacao: any;
  fabricanteSituacao: any;
  produtoValue: any;
  fabricanteArray: any = [];
  fornecedorArray: any = [];
  alerta: boolean;
  alertaModal: boolean;
  classeMensagem: string;
  mensagem: string;
  produtoTabela: boolean;

  constructor(
    private route: ActivatedRoute,
    private service: GeralService
  ) {
    this.produtoValue = null;
    this.fabricanteModel = new FabricanteModel();
    this.fornecedorModel = new FornecedorModel();
    this.route.params.subscribe( params => {
      if(params.id){
          this.titulo = "Editar Produto";
      }
      else{
          this.titulo = "Produto";
      }
  });    
  }

  ngOnInit() {
    this.getTodosFornecedores();
    this.getTodosFabricantes();
    this.getTodosProdutos();
  }

  getTodosFornecedores(){
    this.fornecedorSituacao = null;
    this.service.getTodosFornecedores().subscribe((data) => {
      this.fornecedorArray = data['fornecedores'];
    })
  }

  abreModalFabricante(){
    $('#modalFabricante').modal('show');
    this.fabricanteModel = {};
    this.alertaModal = false;
  }

  abreModalFornecedor(){
    $('#modalFornecedor').modal('show');
    this.fornecedorModel = {};
    this.alertaModal = false;
  }

  salvarFabricante(){
    this.service.postCadastraFabricante(this.fabricanteModel).subscribe((response) => {
      if(response['sucesso']){
        this.verificaMensagemAlerta(response['sucesso'], response['mensagem']);
        this.getTodosFabricantes();
      }
    })
  }

  salvarFornecedor(){
    this.service.postCadastraFornecedor(this.fornecedorModel).subscribe((response) => {
      if(response['sucesso']){
        this.verificaMensagemAlerta(response['sucesso'], response['mensagem']);
        this.getTodosFornecedores();
      }
    })
  }

  getProdutoCadastrado(id){
    if(id === null){
      return;
    }
    if(parseInt(id) == 0){
      this.alerta = false;
      this.produtoTabela = true;
      this._abrirFormNovoProduto = false;
      this.getTodosProdutos();
    }
    else{
      this.produtoTabela = false;
      this.alerta = false;
      this.abrirFormNovoProduto();
      this.service.getProdutoId(id).subscribe(data => {
        this.produtoModel = data['produto'];
        this.fabricanteSituacao = data['produto']['fabricante_id'];
        this.fornecedorSituacao = data['produto']['fornecedor_id'];
      })

    }
  }

  getTodosFabricantes(){
    this.fabricanteSituacao = null;
    this.service.getTodosFabricantes().subscribe((data) => {
      this.fabricanteArray = data['fabricantes'];
    })
  }

  getTodosProdutos(){
    this.produtoSituacao = null;
    this.service.getTodosProdutos().subscribe((data: any[]) => {
      this.produtoSituacao = null;
      this.produtoArray = data['produtos'];
      console.log('this',this.produtoArray);
    })
  }

  typeaheadOnSelect($event){
    console.log('event', $event);
    this.getProdutoCadastrado($event.item.id);
}

  getFabricanteCadastrado(id){
    this.produtoModel.fabricante_id = id;
  }

  getFornecedorCadastrado(id){
    this.produtoModel.fornecedor_id = id;
  }

  abrirFormNovoProduto(){
    this._abrirFormNovoProduto = true;
    this.alerta = false;
    this.produtoTabela = false;
    this.produtoModel = {};
    this.fabricanteSituacao = null;
    this.fornecedorSituacao = null;
  }

  getProdutoSituacao(event?){
    console.log(event);
    this.produtoValue = null;
    this.alerta = false;
    if(event === 'null'){
      return;
    }
    else{
      this.service.getProdutoSituacao(parseInt(event)).subscribe(data => {
        console.log(data)
        this.produtoArray = data['produtos'];
    })
    }
  }

  cadastrar(){
    if(!this.produtoModel.codigo){
      alert('O campo código é obrigatório');
      return;
    }
    if(!this.produtoModel.nome){
      alert('O campo nome é obrigatório');
      return;
    }
    if(this.produtoModel.fabricante_id == null){
      alert('O campo fabricante é obrigatório');
      return;
    }
    if(this.produtoModel.fornecedor_id == null){
      alert('O campo fornecedor é obrigatório');
      return;
    }
    if(!this.produtoModel.estoque){
      alert('O campo estoque é obrigatório');
      return;
    }
    if(!this.produtoModel.preco_custo){
      alert('O preço de custo é obrigatório');
      return;
    }
    if(!this.produtoModel.preco_venda){
      alert('O preço de venda é obrigatório');
      return;
    }
    this.verificaMensagemAlerta(false, '');
    this.service.postCadastraAtualizaProduto(this.produtoModel).subscribe((response) => {
      if(response['sucesso'] == true){
        this.verificaMensagemAlerta(true, response['mensagem']);
        this.produtoModel = {};
        this.alerta = true;
        this.produtoSituacao = null;
        this.fornecedorSituacao = null;
        this.fabricanteSituacao = null;
        this._abrirFormNovoProduto = false;
        this.getTodosProdutos();
        this.getTodosFabricantes();
        this.getTodosFornecedores();
      }
      else{
        this.verificaMensagemAlerta(false, response['mensagem']);
      }
    })
  }

  limpar(){
    this.produtoModel.codigo = '';
    this.produtoModel.estoque = '';
    this.produtoModel.informacoes = '';
    this.produtoModel.nome = '';
    this.produtoModel.preco_custo = '';
    this.produtoModel.preco_venda = '';
    this.produtoModel.fabricante_id = null;
    this.produtoModel.fornecedor_id = null;
    this.fornecedorSituacao = null;
    this.fabricanteSituacao = null;
  }

  verificaMensagemAlerta(tipo: boolean, msg: string){
    if(tipo){
        this.classeMensagem = 'alert-success';
        this.alertaModal = true;
    }
    else{
        this.classeMensagem = 'alert-danger';
        this.alertaModal = true;
    }
    this.mensagem = msg;
}

inativaAtivaProduto(produto: any){
  let mensagem;
  if(produto.ativo == 0){
      mensagem = 'Deseja realmente desativar o cliente ' + produto.nome + ' ?'
  }
  else{
      mensagem = 'Deseja realmente ativar o cliente ' + produto.nome + ' ?'
  }
  if(window.confirm(mensagem)){            
      this.service.inativaAtivaProduto(produto).subscribe((data) => {
          this.produtoValue = null;
          this.verificaMensagemAlerta(data['sucesso'], data['mensagem']);
          //this.getClienteSituacao(cliente.ativo);
          this.getTodosProdutos();
      })
  }
}

}
