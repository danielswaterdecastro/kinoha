export class ClienteModel {
    id?: number;
    nome?: any;
    empresa?: any;
    endereco?: any;
    cidade?: any;
    telefone?: any;
    email?: any;
    informacoes?: string;
    ativo?: any;
    data_cadastro?: any;
    data_atualizacao?: any;
}