export class VendaModel{
    id?: number;
    valor_total?: any;
    valor_subtotal?: any;
    forma_pagamento?: any;
    informacao_forma_pagamento?: any;
    informacao?: any;
    nome?: any;
    data?: any;
    cidade?: any;
    venda_data?: any;
    tipo_venda_data?: any;
    data_retroativa?:any

    //Clientes
    id_cliente?: number;
/*     empresa?: any;
    endereco?: any;
    telefone?: any;
    email?: any;
    nome_cliente: string; */

    produtoArray?: any[];

    //Produtos
    id_produto?: number;
    codigo?: string;
    estoque?: any;
    quantidade?: any;
    nome_produto?: string;
    nome_fornecedor?: string;
    nome_fabricante?: string;
    preco_custo?: any;
    preco_venda?: any;
    quitado?: any;
}