export class ProdutoModel {
    id?: number;
    codigo?: string;
    nome?: string;
    fabricante_id?: number;
    fornecedor_id?: number;
    nome_fornecedor?: string;
    nome_fabricante?: string;
    estoque?: any;
    preco_custo?: any;
    preco_venda?: any;
    ativo?: number;
    informacoes?: string;
    data_cadastro?: any;
    data_atualizacao?: any;
}