import { Routes } from '@angular/router';

import { DashboardComponent }   from './dashboard/dashboard.component';
import { UserComponent }   from './user/user.component';
import { ProdutoComponent }   from './produto/produto.component';
import { TypographyComponent }   from './typography/typography.component';
import { IconsComponent }   from './icons/icons.component';
import { NotificationsComponent }   from './notifications/notifications.component';

import { VendaComponent } from './venda/venda.component';
import { FinanceiroComponent } from './financeiro/financeiro.component';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'cliente',
        pathMatch: 'full',
    },
    {
        path: 'dashboard',
        component: DashboardComponent
    },
    {
        path: 'cliente',
        component: UserComponent
    },
    {
        path: 'produto',
        component: ProdutoComponent
    },
    {
        path: 'venda',
        component: VendaComponent
    },
    {
        path: 'financeiro',
        component: FinanceiroComponent
    },
    {
        path: 'icons',
        component: IconsComponent
    },
    {
        path: 'notifications',
        component: NotificationsComponent
    }
]
