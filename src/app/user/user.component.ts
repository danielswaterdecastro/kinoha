import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { ClienteModel } from '../models/clienteModel';
import { GeralService } from './../geral.service';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
//import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';

declare var $:any;

@Component({
    selector: 'user-cmp',
    moduleId: module.id,
    templateUrl: 'user.component.html'
})

export class UserComponent implements OnInit{

    titulo: string;
    selected: string
    _abrirFormNovoCliente: boolean = false;
    clienteModel: ClienteModel;
    clienteValue: any;
    clienteSituacao: any;
    clienteArray: ClienteModel[];
    clienteTabela: boolean;
    clienteTabelaProduto: boolean;
    clienteTabelaProdutoArray: Array<any> = [];
    classeMensagem: any;
    mensagem: string;
    alerta: boolean;
    habilitado: boolean;

    constructor(
        private route: ActivatedRoute,
        private service: GeralService,
        ){
        this.clienteValue = null;
        this.clienteSituacao = null;
        this.clienteModel = new ClienteModel();
        this.route.params.subscribe( params => {
            if(params.id){
                this.titulo = "Editar Cliente";
            }
            else{
                this.titulo = "Cliente";
            }
        });
    }
    ngOnInit(){
        //alert($('.header').html());
        this.getTodosClientes();
    }

    abrirFormNovoCliente(){
        this.verificaMensagemAlerta(false, '');
        this._abrirFormNovoCliente = true;
        this.clienteModel = {};
        this.clienteValue = null;
        this.clienteSituacao = null;
        this.clienteTabela = false;
        this.alerta = false;
    }
    cadastrar(){
        if(!this.clienteModel.nome){
            alert('O campo nome é obrigatório');
            return;
        }
        if(!this.clienteModel.empresa){
            alert('O campo empresa é obrigatório');
            return;
        }
        this.verificaMensagemAlerta(false, '');
        this.service.postCadastraAtualizaCliente(this.clienteModel).subscribe((response) => {
            console.log('response', response);
            if(response['sucesso'] == true){
                this.verificaMensagemAlerta(true, response['mensagem']);
                this.clienteModel = {};
                this.getTodosClientes();
                this.clienteValue = null;
                this.clienteSituacao = null;
                this._abrirFormNovoCliente = false;
            }
            else{
                this.verificaMensagemAlerta(false, response['mensagem']);
            }
        })
    }

    limpar(){
        this.clienteModel.nome = '';
        this.clienteModel.telefone = '';
        this.clienteModel.empresa = '';
        this.clienteModel.email = '';
        this.clienteModel.informacoes = '';
        this.clienteModel.endereco = '';
        this.clienteModel.cidade = '';
        //this.clienteModel = {};
        this.clienteValue = null;
        this.clienteSituacao = null;
    }

    getClienteSituacao(event?){
        this.clienteValue = null;
        this.alerta = false;
        if(event === 'null'){
            //this.getTodosClientes();
            return;
        }
        else{
            this.service.getClienteSituacao(parseInt(event)).subscribe(data => {
                this.clienteArray = data['clientes'];
            })
        }
    }

    getTodosClientes(){
        this.clienteSituacao = null;
        this.service.getTodosClientes().subscribe((data: ClienteModel[]) => {
            this.clienteArray = data['clientes'];
            console.log(this.clienteArray);
        })
    }

    typeaheadOnSelect($event){
        console.log('event', $event);
        this.getClienteCadastrado($event.item.id);
    }

    getClienteProdutos(idCliente){
        this.service.getClienteProdutos(idCliente).subscribe(data => {
            this.clienteTabelaProduto = true;
            this.clienteTabelaProdutoArray = data['produtosCliente'];
            console.log('clienteTabelaProdutoArray', this.clienteTabelaProdutoArray);
        })
    }

    getClienteCadastrado(id): void {
        if(id === 'null'){
            return;
        }
        if(parseInt(id) == 0){
            this.alerta = false;
            this.clienteTabela = true;
            this._abrirFormNovoCliente = false;
            this.getTodosClientes();
        }
        else{
            this.clienteTabela = false;
            this.abrirFormNovoCliente();
            this.service.getClienteId(id).subscribe(data => {
                this.clienteModel = data['cliente'];
                this.getClienteProdutos(this.clienteModel.id);
                if(this.clienteModel.ativo == 1){
                    this.habilitado = false;
                }
            })
        }
    }

    verificaMensagemAlerta(tipo: boolean, msg: string){
        if(tipo){
            this.classeMensagem = 'alert-success';
            this.alerta = true;
        }
        else{
            this.classeMensagem = 'alert-danger';
            this.alerta = true;
        }
        this.mensagem = msg;
    }

    inativaAtivaCliente(cliente: any){
        let mensagem;
        if(cliente.ativo == 0){
            mensagem = 'Deseja realmente desativar o cliente ' + cliente.nome + ' ?'
        }
        else{
            mensagem = 'Deseja realmente ativar o cliente ' + cliente.nome + ' ?'
        }
        if(window.confirm(mensagem)){
            this.service.inativaCliente(cliente).subscribe((data) => {
                this.clienteValue = null;
                this.verificaMensagemAlerta(data['sucesso'], data['mensagem']);
                //this.getClienteSituacao(cliente.ativo);
                this.getTodosClientes();
            })
        }
    }
}
