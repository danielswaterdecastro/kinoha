import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

//const API = '/sistema/';
const API = 'http://localhost/sistema/';

@Injectable()
export class GeralService {

  constructor(private http: HttpClient) { }

  // Módulo Clientes
  postCadastraAtualizaCliente(cliente: any){
    return this.http.post(API + 'cadastra-atualiza-cliente', cliente);
  }

  getTodosClientes(){
    return this.http.get(API + 'todos-clientes');
  }

  getClienteProdutos(idCliente){
    return this.http.get(API + 'cliente-produtos/' + idCliente);
  }
  
  getClienteId(idCliente){
    return this.http.get(API + 'clientes-id/' + idCliente);
  }

  getClienteSituacao(situacao){
    return this.http.get(API + 'cliente-situacao/' + situacao);
  }

  inativaCliente(cliente: any){
    return this.http.post(API + 'inativa-ativa-cliente', cliente);
  }

  // Módulo Fabricante

  postCadastraFabricante(fabricante: any){
    return this.http.post(API + 'cadastra-fabricante', fabricante);
  }

  // Módulo Fornecedor

  postCadastraFornecedor(fornecedor: any){
    return this.http.post(API + 'cadastra-fornecedor', fornecedor);
  }

  // Modulo Produtos

  getTodosFornecedores(){
    return this.http.get(API + 'todos-fornecedores');
  }

  getTodosFabricantes(){
    return this.http.get(API + 'todos-fabricantes');
  }

  getTodosProdutos(){
    return this.http.get(API + 'todos-produtos');
  }

  getProdutoId(id){
    return this.http.get(API + 'produtos-id/' + id);
  }

  getProdutoSituacao(situacao){
    return this.http.get(API + 'produto-situacao/' + situacao);
  }

  postCadastraAtualizaProduto(produto){
    return this.http.post(API + 'cadastra-atualiza-produto', produto);
  }

  inativaAtivaProduto(produto: any){
    return this.http.post(API + 'inativa-ativa-produto', produto);
  }

  // Módulo Vendas

  getFabricanteByProdutoId(id){
    return this.http.get(API + 'fabricante-by-produto-id/' + id);
  }

  getFabricanteNome(idProduto){
    return this.http.get(API + 'fabricante-nome/' + idProduto);
  }

  getFornecedorNome(idProduto){
    return this.http.get(API + 'fornecedor-nome/' + idProduto);
  }

  getFornecedorByProdutoId(id){
    return this.http.get(API + 'fornecedor-by-produto-id/' + id);
  }

  postVenda(venda: any){
    return this.http.post(API + 'vendas', venda);
  }
  getTodasVendas(){
    return this.http.get(API + 'todas-vendas');
  }

  getRecibo(id){
    return this.http.get(API + 'recibo/' + id);
  }

  //Módulo financeiro

  getVendaByCliente(id){
    return this.http.get(API + 'venda-by-cliente/' + id);
  }

  postQuitarVenda(venda: any){
    return this.http.post(API + 'quitar-venda', venda);
  }

  getProdutoMaisVendido(){
    return this.http.get(API + 'produto-mais-vendido');
  }

  postProdutoData(datas: any){
    return this.http.post(API + 'filtra-produto-data', datas);
  }

}
